package org.example.controller;
 
import com.alibaba.fastjson.JSONObject;
import org.example.entity.Person;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
 
/**
 * @author ryzhang5
 * @Package com.ryzhang.controller
 * @date 2019/9/22 11:31
 * @Copyright
 */
@Controller
public class TestController {
 
 
    @RequestMapping("/test" )
    @ResponseBody
    public Person test(){
        Person person = new Person("小明",18);
        System.out.println(JSONObject.toJSONString(person));
        System.out.println("测试");
        return  person;
    }
}