package org.example;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(org.example.SpringBootServiceApplication.class, args);
    }

}
