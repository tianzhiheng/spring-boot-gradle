package org.example.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ryzhang5
 * @Package com.ryzhang.model
 * @date 2019/9/22 11:29
 * @Copyright
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Person {
    private String name;
    private Integer age;
 
}